package com.epam.pizza.order.factory;

import com.epam.pizza.order.Bakeries;
import com.epam.pizza.order.PizzaType;
import com.epam.pizza.order.bakery.Bakery;
import com.epam.pizza.order.bakery.DniproBakery;
import com.epam.pizza.order.bakery.KyivBakery;
import com.epam.pizza.order.bakery.LvivBakery;

public class PizzaOrder extends AbstractBakeryPizzaOrder {

  private Bakery chooseLvivBakeryPizza(PizzaType pizzaType) {
    Bakery bakery = null;
    if (pizzaType == PizzaType.CHEESE) {
      bakery = new LvivBakery(pizzaType);
    } else if (pizzaType == PizzaType.VEGGIE) {
      bakery = new LvivBakery(pizzaType);
    } else if ((pizzaType == PizzaType.PEPPERONI)) {
      bakery = new LvivBakery(pizzaType);
    } else if (pizzaType == PizzaType.CLAM) {
      bakery = new LvivBakery(pizzaType);
    }
    return bakery;
  }

  private Bakery chooseDniproBakeryPizza(PizzaType pizzaType) {
    Bakery bakery = null;
    if (pizzaType == PizzaType.CHEESE) {
      bakery = new DniproBakery(pizzaType);
    } else if (pizzaType == PizzaType.VEGGIE) {
      bakery = new DniproBakery(pizzaType);
    } else if ((pizzaType == PizzaType.PEPPERONI)) {
      bakery = new DniproBakery(pizzaType);
    } else if (pizzaType == PizzaType.CLAM) {
      bakery = new DniproBakery(pizzaType);
    }
    return bakery;
  }

  private Bakery chooseKyivBakeryPizza(PizzaType pizzaType) {
    Bakery bakery = null;
    if (pizzaType == PizzaType.CHEESE) {
      bakery = new KyivBakery(pizzaType);
    } else if (pizzaType == PizzaType.VEGGIE) {
      bakery = new KyivBakery(pizzaType);
    } else if ((pizzaType == PizzaType.PEPPERONI)) {
      bakery = new KyivBakery(pizzaType);
    } else if (pizzaType == PizzaType.CLAM) {
      bakery = new KyivBakery(pizzaType);
    }
    return bakery;
  }

  public Bakery chooseBakeryPizza(Bakeries bakeries, PizzaType pizzaType) {
    Bakery bakery = null;
    if (bakeries == Bakeries.LVIV) {
      bakery = chooseLvivBakeryPizza(pizzaType);
    } else if (bakeries == Bakeries.DNIPRO) {
      bakery = chooseDniproBakeryPizza(pizzaType);
    } else if (bakeries == Bakeries.KYIV) {
      bakery = chooseKyivBakeryPizza(pizzaType);
    }
    return bakery;
  }
}
