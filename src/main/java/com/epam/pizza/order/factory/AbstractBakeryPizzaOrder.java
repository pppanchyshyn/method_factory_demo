package com.epam.pizza.order.factory;

import com.epam.pizza.order.Bakeries;
import com.epam.pizza.order.PizzaType;
import com.epam.pizza.order.bakery.Bakery;

public abstract class AbstractBakeryPizzaOrder {

  abstract Bakery chooseBakeryPizza(Bakeries bakeries, PizzaType pizzaType);

  public void getPizza(Bakeries bakeries, PizzaType pizzaType) {
    Bakery bakery = chooseBakeryPizza(bakeries, pizzaType);
    bakery.prepare();
    bakery.bake();
    bakery.cut();
    bakery.box();
    bakery.send();
  }
}
