package com.epam.pizza.order;

import com.epam.pizza.order.factory.AbstractBakeryPizzaOrder;
import com.epam.pizza.order.factory.PizzaOrder;

public class Client {

  public static void main(String[] args) {
    AbstractBakeryPizzaOrder order = new PizzaOrder();
    order.getPizza(Bakeries.DNIPRO, PizzaType.CHEESE);
    order.getPizza(Bakeries.LVIV, PizzaType.CHEESE);
    order.getPizza(Bakeries.KYIV, PizzaType.CHEESE);
  }
}
