package com.epam.pizza.order;

public enum PizzaType {
  CHEESE, VEGGIE, CLAM, PEPPERONI
}
