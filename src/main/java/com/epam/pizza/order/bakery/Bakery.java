package com.epam.pizza.order.bakery;

public interface Bakery {

  void prepare();

  void bake();

  void cut();

  void box();

  void send();
}
