package com.epam.pizza.order.bakery;

import com.epam.pizza.order.PizzaType;

public class LvivBakery extends AbstractBakery {

  public LvivBakery(PizzaType pizzaType) {
    super(pizzaType);
  }

  public void prepare() {
    if (pizzaType == PizzaType.CHEESE) {
      System.out.println("Adding thin crust, adding Marinara souse, adding Dutch cheese");
    } else if (pizzaType == PizzaType.VEGGIE) {
      System.out.println("Adding thin crust, adding Plum Tomato souse, adding onion");
    } else if (pizzaType == PizzaType.PEPPERONI) {
      System.out.println("Adding thick crust, adding Presto souse, adding pepper");
    } else if (pizzaType == PizzaType.CLAM) {
      System.out.println("Adding thick crust, adding Marinara souse, adding shrimps");
    }
  }

  public void bake() {
    if (pizzaType == PizzaType.CHEESE) {
      System.out.println("Baking 12 minutes");
    } else if (pizzaType == PizzaType.VEGGIE) {
      System.out.println("Baking 10 minutes");
    } else if (pizzaType == PizzaType.PEPPERONI) {
      System.out.println("Baking 15 minutes");
    } else if (pizzaType == PizzaType.CLAM) {
      System.out.println("Baking 17 minutes");
    }
  }
}
