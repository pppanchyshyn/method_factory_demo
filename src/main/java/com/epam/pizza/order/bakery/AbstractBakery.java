package com.epam.pizza.order.bakery;

import com.epam.pizza.order.PizzaType;

public abstract class AbstractBakery implements Bakery {

  PizzaType pizzaType;

  AbstractBakery(PizzaType pizzaType) {
    this.pizzaType = pizzaType;
  }

  public void cut() {
    System.out.println(String.format("%s pizza is cutting", pizzaType.name()));
  }

  public void box() {
    System.out.println(String.format("%s pizza is boxing", pizzaType.name()));
  }

  public void send() {
    System.out.println(String.format("%s pizza has been sent", pizzaType.name()));
  }
}
