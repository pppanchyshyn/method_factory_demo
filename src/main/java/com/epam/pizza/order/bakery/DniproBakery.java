package com.epam.pizza.order.bakery;

import com.epam.pizza.order.PizzaType;


public class DniproBakery extends AbstractBakery {

  public DniproBakery(PizzaType pizzaType) {
    super(pizzaType);
  }

  public void prepare() {
    if (pizzaType == PizzaType.CHEESE) {
      System.out.println("Adding thick crust, adding Marinara souse, adding Parmesan");
    } else if (pizzaType == PizzaType.VEGGIE) {
      System.out.println("Adding thin crust, adding Plum Tomato souse, adding cucumber");
    } else if (pizzaType == PizzaType.PEPPERONI) {
      System.out.println("Adding thin crust, adding Presto souse, adding pepper");
    } else if (pizzaType == PizzaType.CLAM) {
      System.out.println("Adding thin crust, adding Marinara souse, adding Trout fillet");
    }
  }

  public void bake() {
    if (pizzaType == PizzaType.CHEESE) {
      System.out.println("Baking 11 minutes");
    } else if (pizzaType == PizzaType.VEGGIE) {
      System.out.println("Baking 13 minutes");
    } else if (pizzaType == PizzaType.PEPPERONI) {
      System.out.println("Baking 16 minutes");
    } else if (pizzaType == PizzaType.CLAM) {
      System.out.println("Baking 15 minutes");
    }
  }
}
