package com.epam.pizza.order.bakery;

import com.epam.pizza.order.PizzaType;

public class KyivBakery extends AbstractBakery {

  public KyivBakery(PizzaType pizzaType) {
    super(pizzaType);
  }

  public void prepare() {
    if (pizzaType == PizzaType.CHEESE) {
      System.out.println("Adding thin crust, adding Marinara souse, adding Feta cheese");
    } else if (pizzaType == PizzaType.VEGGIE) {
      System.out
          .println("Adding thin crust, adding Plum Tomato souse, adding cucumber and tomatoes");
    } else if (pizzaType == PizzaType.PEPPERONI) {
      System.out.println("Adding thick crust, adding Presto souse, adding chilly pepper");
    } else if (pizzaType == PizzaType.CLAM) {
      System.out.println("Adding thin crust, adding Marinara souse, adding Salmon fillet");
    }
  }

  public void bake() {
    if (pizzaType == PizzaType.CHEESE) {
      System.out.println("Baking 12 minutes");
    } else if (pizzaType == PizzaType.VEGGIE) {
      System.out.println("Baking 11 minutes");
    } else if (pizzaType == PizzaType.PEPPERONI) {
      System.out.println("Baking 14 minutes");
    } else if (pizzaType == PizzaType.CLAM) {
      System.out.println("Baking 16 minutes");
    }
  }
}
